﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Scaler_WeightDetect : MonoBehaviour{
    public static Scaler_WeightDetect m_Instance;

    public float m_CurrentWeight;
    public float m_MaxWeight;
    public GameObject m_ScaleIndicator;

    float m_TargetPosition;
    float m_NeedleRotation;
    void Start(){
        m_Instance = this;
    }

    void Update(){
        m_TargetPosition = (m_CurrentWeight / m_MaxWeight) * -1.6f;
        if (m_TargetPosition <= -1.6f) m_TargetPosition = -1.6f;
        f_Damper(m_TargetPosition,0.05f);

        m_NeedleRotation = (this.transform.localPosition.y / -1.6f) * -360f;
        m_ScaleIndicator.transform.localEulerAngles = new Vector3(0, 0, m_NeedleRotation);
    }

    void f_Damper(float p_TargetPosition, float p_DamperSpeed) {
        if(this.transform.localPosition.y < p_TargetPosition) {
            this.transform.localPosition = new Vector3(0, this.transform.localPosition.y + p_DamperSpeed, 0);
        }
        if(this.transform.localPosition.y > p_TargetPosition) {
            this.transform.localPosition = new Vector3(0, this.transform.localPosition.y - p_DamperSpeed, 0);
        }
        if (m_CurrentWeight <= 0) {
            this.transform.localPosition = new Vector3(0, 0, 0);
        }
        if (this.transform.localPosition.y < -1.6f) {
            this.transform.localPosition = new Vector3(0, -1.6f, 0);
        }
    }
}
