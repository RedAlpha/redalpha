﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Object_InstanceManager : MonoBehaviour{
    public float m_Weight;
    public bool m_IsWeighed;
    public void OnCollisionStay(Collision collision) {
        if (collision.gameObject.name == "Plate" && !m_IsWeighed) {
            Scaler_WeightDetect.m_Instance.m_CurrentWeight += m_Weight;
            m_IsWeighed = true;
        }
        if (collision.gameObject.tag == "Foods" && !m_IsWeighed) {
            if (collision.gameObject.GetComponent<Object_InstanceManager>().m_IsWeighed) {
                Scaler_WeightDetect.m_Instance.m_CurrentWeight += m_Weight;
                m_IsWeighed = true;
            }
        }
    }

    public void OnMouseDown() {
        Scaler_WeightDetect.m_Instance.m_CurrentWeight -= m_Weight;
        m_IsWeighed = false;
        this.gameObject.SetActive(false);
    }
}
