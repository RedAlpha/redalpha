﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager_ObjectSpawner : MonoBehaviour{
    public GameObject m_ObjectContainer;
    public GameObject m_Object;
    public GameObject m_GreenIndicator;

    public int m_MaxObject;
    public float m_GreenRotation;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (m_ObjectContainer.transform.childCount < m_MaxObject) {
            Instantiate(m_Object, m_ObjectContainer.transform);
        }

        m_GreenIndicator.transform.localEulerAngles = new Vector3(0, 0, m_GreenRotation);
    }
}
